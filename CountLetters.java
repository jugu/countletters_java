class CountLetters {

    private int alphabet[] = new int[abc];
    static final private int cz = 123;
    static final private int ca = 97;
    static final private int abc = cz - ca;

    CountLetters() {
        for(int i = 0; i < abc; i++) {
            alphabet[i] = ca + i;
        }
    }
    
    public void printAlphabet() {
        for(int i = 0; i < alphabet.length; i++) {
            System.out.print((char)alphabet[i] + " ");
        }
    }
    
    public void printArray() {
        for(int i = 0; i < alphabet.length; i++) {
            System.out.print(alphabet[i] + " ");
        }
    }

public static void main(String args[]) {
    System.out.println("");
    CountLetters cl = new CountLetters();
    cl.printAlphabet();
    System.out.println("");
    cl.printArray();
    System.out.println("");
}

}
